require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')
const http = require('http')
const bodyParser = require('body-parser')
const exphbs = require('express-handlebars')
const path = require('path')
const cors = require('cors')

const app = express()
var server = http.createServer(app);

mongoose.connect(process.env.MONGODB_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err)=> {
    if (err) throw err
    console.log('Database connected..')
})

//Cors
app.use(cors())

app.use( bodyParser.json({ limit: '50MB'}) )

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE")
    next()
})

//Routes
let routes = require('./routes/index')
app.use(routes)

// View engine
app.set('views', path.join(__dirname, 'views'))
app.engine('handlebars', exphbs({
    defaultLayout: 'layout'
}))
app.set("view engine", "handlebars")

//Port set
app.set('port', process.env.PORT || 80)
server.listen(app.get('port'), (err) => {
    if (err) throw err;
    console.log('Listening to port ', app.get('port'))
})