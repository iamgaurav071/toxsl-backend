var jwt = require('jsonwebtoken');
const User = require('../models/userModel')

module.exports.authMiddleware = (req, res, next) => {
    if (req.headers.authorization) {
        jwt.verify(req.headers.authorization, process.env.SECRET, function (err, decode) {
            if (err) {
                res.status(500).send({
                    success: false,
                    message: "Invaild token"
                });

            } else {
                User.findOne({
                    email: decode.email,
                    password: decode.password
                }, (err, user) => {
                    if (err) {
                        res.status(500).send({
                            success: false,
                            message: "Invaild token"
                        });
                        return false;
                    } else {
                        next();
                    }
                })
            }
        })
    } else {
        res.status(500).send({
            success: false,
            message: "Invaild Authorization"
        });
    }
}