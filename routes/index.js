const router = require('express').Router()
const UserController = require('../controllers/userController')
const ListController = require('../controllers/listController')
const AuthMiddleWare = require('../middleware/authMiddleware')

router.get('/', (req, res)=> {
    res.render('index', {
        'layout': null
    })
})

router.post('/signupuser', UserController.signUpUser)
router.post('/authenticateuser', UserController.authenticateUser)

router.post('/savenote', AuthMiddleWare.authMiddleware, ListController.saveNotes)
router.get('/getallnotes', AuthMiddleWare.authMiddleware, ListController.getAllNotes)
router.post('/deletenote', AuthMiddleWare.authMiddleware, ListController.deleteNote)
router.post('/getlistbyid', AuthMiddleWare.authMiddleware, ListController.getListById)
router.post('/updatenote', AuthMiddleWare.authMiddleware, ListController.updateNote)

module.exports = router