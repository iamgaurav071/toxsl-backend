"use strict";
const mongoose = require('mongoose');

const noteListSchema = mongoose.Schema({
    title: {
        type: String
    },
    author: {
        type: String
    },
    note: {
        type: String
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Admin'
    },
    created_on: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model("List", noteListSchema);