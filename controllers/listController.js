const List = require('../models/listModel')

module.exports = {
    async saveNotes(req, res) {
        try {
            let data = req.body

            let saveNote = new List()

            saveNote.title = data.title
            saveNote.author = data.author
            saveNote.note = data.note
            saveNote.created_by = req.headers.userid

            await saveNote.save((err) => {
                if (err) throw err
                res.status(200).json({
                    success: true,
                    message: 'note saved..'
                })
            })
        } catch (err) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + err
            })
        }
    },

    async getAllNotes(req, res) {
        try {
            let getNotes = await List.find({created_by: req.headers.userid})

            res.status(200).json({
                success: true,
                data: getNotes
            })

        } catch (error) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + error
            })
        }
    },

    async getListById(req, res) {
        try {
           let data = await List.findOne({_id: req.body.id, created_by: req.headers.userid})

           res.status(200).json({
                success: true,
                data: data
            })
        } catch (error) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + error
            })
        }
    },

    async updateNote(req, res) {
        try {
            await List.updateOne({_id: req.body.id, created_by: req.headers.userid},{
                title: req.body.title,
                author: req.body.author,
                note: req.body.note
            }, (err)=> {
                if (err) throw err
                res.status(200).json({
                    success: true,
                    msg: 'updated..'
                })
            })
            
        } catch (error) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + error
            })
        }

    },

    async deleteNote(req, res){
        try {
            await List.deleteOne({_id: req.body.id, created_by: req.headers.userid}, (err)=> {
                if (err) throw err
                res.status(200).json({
                    success: true,
                    msg: 'Deleted successfully'
                })
            })
        } catch (error) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + error
            })
        }
    }
}