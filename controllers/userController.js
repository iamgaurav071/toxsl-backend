const User = require('../models/userModel')
const jwt = require("jsonwebtoken");
var _ = require("lodash");

module.exports = {
    async signUpUser(req, res) {
        try {
            let data = req.body

            let existingUserByEmail = await User.findOne({
                email: data.email
            })

            if (!existingUserByEmail) {
                let saveUser = new User();

                saveUser.name = data.name
                saveUser.email = data.email
                saveUser.password = data.password

                await saveUser.save((err) => {
                    if (err) throw err
                    res.status(200).json({
                        success: true,
                        message: 'user registered..'
                    })
                })
            } else {
                res.status(200).json({
                    success: false,
                    message: 'duplicate-email'
                })
            }


        } catch (err) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + err
            })
        }
    },

    async authenticateUser(req, res) {
        try {
            let email = req.body.email
            let password = req.body.password

            await User.findOne({
                email: email
            }).exec((err, user) => {
                if (err) throw err;
                if (!user) {
                    res.status(200).json({
                        success: false,
                        message: 'email-password missmatch'
                    })
                } else {
                    user.comparedPassword(password, (err, isMatch) => {
                        if (isMatch && !err){
                            const userPick = _.pick(user, [
                                "name", "email", "password"
                            ])

                            const token = jwt.sign(userPick, process.env.SECRET, {
                                expiresIn: '1d' //1 day
                            });

                            res.status(200).json({
                                success: true,
                                token: token,
                                user: {
                                    id: user._id,
                                    email: user.email,
                                    name: user.name
                                }
                            })
                        } else {
                            res.status(200).json({
                                success: false,
                                message: 'email-password missmatch'
                            })
                        }
                    })
                }
            })
        } catch (err) {
            res.status(500).json({
                success: false,
                message: 'Some think went wrong: ' + err
            })
        }
    }
}